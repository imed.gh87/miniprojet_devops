# Academic Project: Calculator.js: a node.js Demonstration Project
An example node.js project, including tests with mocha, that behaves like a pocket calculator.

The project contains a simple node.js application that exposes REST APIs to perform arithmetic on integers, and provides a test suite with mocha and chai.

To build, simply:

1. pull image from dockerHub
```
docker pull imedgh/calculator-app:latest
```

2. To run docker image tape 
```
docker run -p 3000:3000 imedgh/calculator-app:latest
```

# SecreenShot

## Jenkins

![IMAGE_DESCRIPTION](/img/jenkins.png)

## Docker
![IMAGE_DESCRIPTION](/img/docker.png)


## Start service LoadBalancer
![IMAGE_DESCRIPTION](/img/start_service_loadBalancer.png)

## running Application
![IMAGE_DESCRIPTION](/img/runing_application.png)


## ArgoCD Application
![IMAGE_DESCRIPTION](/img/argo_cd.png)