## creation fichier deployement kubernetes
Pour deployer une application on utilisant kubernetes il faut creer 2 fichiers ("nomFichier-deployement.yaml" et "nomFichier-service.yaml" )

Pour le deployement et les services vous pouvez consultez le site officiel KUBERNETES dedant vous trouverez la documentation necessaire.

## Commandes necessaire
Deploy:
```
kubectl apply -f nomFichier-Deployement.yaml
```

Service:
```
kubectl apply -f nomFichier-service.yaml
```

## Pour consulter votre application
```
minikube service nomFichier-app-service
```
Exemple: minikube service calculator-app-service