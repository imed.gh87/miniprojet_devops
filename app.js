// Importe le module Express.js
const express = require('express');

// Crée une nouvelle application Express.js
const app = express();

// Définit le port sur lequel l'application fonctionnera
app.set('port', 3000);

// Crée un endpoint GET qui renvoie la page d'accueil de la calculatrice
app.get('/', (req, res) => {
  res.render('index.html');
});

// Crée un endpoint POST qui traite les calculs de la calculatrice
app.post('/calculate', (req, res) => {
  // Récupère les données de la requête POST
  const { operand1, operand2, operator } = req.body;

  // Effectue le calcul
  const result = calculate(operand1, operand2, operator);

  // Renvoie le résultat
  res.json({ result });
});

// Fonction qui calcule une expression
function calculate(operand1, operand2, operator) {
  switch (operator) {
    case '+':
      return operand1 + operand2;
    case '-':
      return operand1 - operand2;
    case '*':
      return operand1 * operand2;
    case '/':
      return operand1 / operand2;
    default:
      throw new Error(`Opérateur invalide : ${operator}`);
  }
}

// Lance l'application
app.listen(app.get('port'), () => {
  console.log(`Serveur démarré sur le port ${app.get('port')}`);
});
});